# Latex Snippets #

## Description ##
A Snippet is am minimal example for getting something done in LaTeX.
Minimal means that you can not remove a line of code for showing
the same thing. We try to find the best solution for doing this 
something. Best means to write flexible and comprehensible code
which produces beautiful output.

A good start for creating a new snippet is *template.tex*
